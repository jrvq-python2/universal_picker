# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: external_nuke_commands.py
Date: 2018.12.29
Revision: 2020.01.26
Copyright: Copyright 2018 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2018, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


import nuke
import nukescripts

def rgb_to_nuke_color(r, g, b):
    r_i, g_i, b_i = int(r*255), int(g*255), int(b*255)
    return int('{:02x}{:02x}{:02x}{:02x}'.format(r_i, g_i, b_i, 1), 16)

def nuke_paint_nodes(r, g, b):
    if len(nuke.selectedNodes()) == 0:
        return (False, 'No node selected')

    for node in nuke.selectedNodes():
        node['tile_color'].setValue(rgb_to_nuke_color(r, g, b))

    if len(nuke.selectedNodes()) == 1:
        return (True, 'Node painted correctly')
    else:
        return (True, 'Nodes painted correctly')

def nuke_constant(r, g, b, there_is_alpha, a):
    const = nuke.nodes.Constant()

    if there_is_alpha:
        const['color'].setValue((r, g, b, a))
    else:
        const['color'].setValue((r, g, b, 1))

    return (True, 'Constant node created')

def nuke_backdrop(r, g, b):
    bd = nukescripts.autoBackdrop()
    bd['tile_color'].setValue(rgb_to_nuke_color(r, g, b))
    return (True, 'Backdrop node created')

def nuke_set(input, r, g, b, there_is_alpha, a):
    if len(nuke.selectedNodes()) == 0:

        if '.' not in input:
            return (False, 'You have nothing selected, please try something like "Constant1.color"')

        object = str(input.split('.')[0])
        attribute = str(input.split('.')[1])

        try:
            node = nuke.toNode(object)
            if there_is_alpha:
                node[attribute].setValue((r, g, b, a))
            else:
                node[attribute].setValue((r, g, b, 1))
            return (True, 'Knob value set correctly')
        except Exception as e:
            return (False, 'Could not set knob value')

    elif len(nuke.selectedNodes()) == 1:
        object = nuke.selectedNodes()[0].name()
        attribute = input

        if '.' in input:
            return (False, 'You have a node selected, to set an attribute, please try something like "color"')

        try:
            node = nuke.toNode(object)
            if there_is_alpha:
                node[attribute].setValue((r, g, b, a))
            else:
                node[attribute].setValue((r, g, b, 1))
            return (True, 'Knob value set correctly')
        except Exception as e:
            return (False, 'Could not set knob value')

    elif len(nuke.selectedNodes()) > 1:
        node_number = len(nuke.selectedNodes())

        try:
            for node in nuke.selectedNodes():
                object = node.name()
                attribute = input
                node = nuke.toNode(object)
                if there_is_alpha:
                    node[attribute].setValue((r, g, b, a))
                else:
                    node[attribute].setValue((r, g, b, 1))
            return (True, 'Knob value set correctly to {} selected nodes'.format(node_number))
        except Exception as e:
            return (False, 'Could not set knob value to selected nodes')

def nuke_get(input):
    if len(nuke.selectedNodes()) == 0:

        if '.' not in input:
            return (False, 'You have nothing selected, please try something like "Constant1.color"')

        object = str(input.split('.')[0])
        attribute = str(input.split('.')[1])

        try:
            node = nuke.toNode(object)
            attr = node[attribute].value()
            return (True, str(attr))
        except Exception as e:
            return (False, 'Could not get knob value')

    elif len(nuke.selectedNodes()) == 1:

        object = str(nuke.selectedNodes()[0].name())
        attribute = input

        if '.' in input:
            return (False, 'You have a node selected, to retrieve an attribute, please try something like "color"')

        try:
            node = nuke.toNode(object)
            attr = node[attribute].value()
            return (True, str(attr))
        except Exception as e:
            return (False, 'Could not get knob value')

    elif len(nuke.selectedNodes()) > 1:
        return (False, 'Please, select one node or less')

def nuke_IO(input_text):
    def execute_nuke(executable):
        import nuke
        import os, math, random
        exec (executable)

    try:
        execute_nuke(input_text)
        return (True, 'Command succesfully executed')
    except Exception as e:
        return (False, 'ERROR: ' + str(e))
