# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: external_maya_commands.py
Date: 2018.12.29
Revision: 2020.01.26
Copyright: Copyright 2018 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2018, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


from maya import cmds


def maya_lambert(r, g, b, there_is_alpha, a):
    lm = cmds.shadingNode('lambert', asShader=True)
    sg = cmds.sets(renderable=True, empty=True)

    cmds.connectAttr('{}.outColor'.format(lm), '{}.surfaceShader'.format(sg), force=True)
    cmds.setAttr('{}.color'.format(lm), r, g, b)

    if there_is_alpha:
        fn = cmds.createNode('floatConstant')
        cmds.setAttr('{}.inFloat'.format(fn), 1-a)
        cmds.connectAttr('{}.outFloat'.format(fn), '{}.transparencyR'.format(lm))
        cmds.connectAttr('{}.outFloat'.format(fn), '{}.transparencyG'.format(lm))
        cmds.connectAttr('{}.outFloat'.format(fn), '{}.transparencyB'.format(lm))

    cmds.select(lm)

    return (True, 'lambert shader created')

def maya_aiStandard(r, g, b, there_is_alpha, a):
    ai = cmds.shadingNode('aiStandardSurface', asShader=True)
    sg = cmds.sets(renderable=True, empty=True)

    cmds.connectAttr('{}.outColor'.format(ai), '{}.surfaceShader'.format(sg), force=True)
    cmds.setAttr('{}.baseColor'.format(ai), r, g, b)

    if there_is_alpha:
        fn = cmds.createNode('floatConstant')
        cmds.setAttr('{}.inFloat'.format(fn), a)
        cmds.connectAttr('{}.outFloat'.format(fn), '{}.opacityR'.format(ai))
        cmds.connectAttr('{}.outFloat'.format(fn), '{}.opacityG'.format(ai))
        cmds.connectAttr('{}.outFloat'.format(fn), '{}.opacityB'.format(ai))

    cmds.select(ai)

    return (True, 'aiStandardSurface shader created')

def maya_constant(r, g, b, there_is_alpha, a):
    const = cmds.createNode('colorConstant')
    cmds.setAttr("{}.inColor".format(const), r, g, b)
    if there_is_alpha:
        cmds.setAttr("{}.inAlpha".format(const), a)

    return (True, 'colorConstant node created')

def maya_set(input, r, g, b):
    if len(cmds.ls(selection=True)) == 0:

        if '.' not in input:
            return (False, 'You have nothing selected, please try something like "colorConstant1.inColor"')

        object = str(input.split('.')[0])
        attribute = str(input.split('.')[1])

        try:
            cmds.setAttr("{}.{}".format(object, attribute), r, g, b)
            return (True, 'Attribute set correctly')
        except Exception as e:
            return (False, 'Could not set attribute')

    elif len(cmds.ls(selection=True)) == 1:

        object = str(cmds.ls(selection=True)[0])
        attribute = input

        if '.' in input:
           return (False, 'You have a node selected, to set an attribute, please try something like "inColor"')

        try:
            cmds.setAttr("{}.{}".format(object, attribute), r, g, b)
            return (True, 'Attribute set correctly')
        except Exception as e:
           return (False, 'Could not set attribute')

    elif len(cmds.ls(selection=True)) > 1:

        node_number = len(cmds.ls(selection=True))

        try:
            for node in cmds.ls(selection=True):
                object = str(node)
                attribute = input
                cmds.setAttr("{}.{}".format(object, attribute), r, g, b)
            return (True, 'Attribute set correctly to {} selected nodes'.format(node_number))
        except Exception as e:
           return (False, 'Could not set attribute to selected nodes')

def maya_get(input):
    if len(cmds.ls(selection=True)) == 0:

        if '.' not in input:
            return (False, 'You have nothing selected, please try something like "colorConstant1.inColor"')

        object = str(input.split('.')[0])
        attribute = str(input.split('.')[1])

        try:
            attr = cmds.getAttr("{}.{}".format(object, attribute))
            return (True, str(attr))
        except Exception as e:
           return (False, 'Could not get attribute')

    elif len(cmds.ls(selection=True)) == 1:

        object = str(cmds.ls(selection=True)[0])
        attribute = input

        if '.' in input:
           return (False, 'You have a node selected, to retrieve an attribute, please try something like "inColor"')

        try:
            attr = cmds.getAttr("{}.{}".format(object, attribute))
            return (True, str(attr))
        except Exception as e:
           return (False, 'Could not get attribute')

    elif len(cmds.ls(selection=True)) > 1:
       return (False, 'Please, select one object or less')

def maya_IO(input_text):
    def execute_maya(executable):
        from maya import cmds
        import os, math, random
        exec (executable)
    try:
        execute_maya(input_text)
        return (True, 'Command succesfully executed')
    except Exception as e:
        return (False, 'ERROR: ' + str(e))
